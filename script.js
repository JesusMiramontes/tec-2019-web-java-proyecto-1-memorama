var canvas;
var cells_ids = [0];
var cells_array = [0];
var NROWS = 0;
var NCELLS = 0;
var values_array = [];
var unfolded = [];

var current_pair = [];

function initScript() {
    canvas = document.getElementById("canvas");
    generateValues();
    addNRowsWithMCells(NROWS, NCELLS);
}

function configGame() {
    var nrows = document.getElementById("NROWS");
    var ncells = document.getElementById("NCELLS");
    NROWS = nrows[nrows.selectedIndex].value;
    NCELLS = ncells[ncells.selectedIndex].value;

    cells_ids = [0];
    cells_array = [0];
    values_array = [];
    unfolded = [];

    removeChildDom(document.getElementById("canvas"));
    initScript();
}

/**
 * Agrega n filas al canvas
 * @param nrows
 */
function addRows(nrows) {
    for(var i = 0; i < nrows; i++){
        canvas.appendChild(createItem("row"));
    }
}

/**
 * Agrega celdas a una fila
 * @param row fila a la que se agregará
 * @param ncells número celdas a agregar
 */
function addCells(row, ncells) {
    for(var i = 0; i < ncells; i++){
        row.appendChild(createItem("cell"));
    }
}

/**
 * Agrega al DOM n filas con m celdas cada una
 * @param nrows Número de filas
 * @param ncells Número de celdas
 */
function addNRowsWithMCells(nrows, ncells) {
    addRows(nrows);

    var rows = document.getElementsByClassName("row");

    for(var i = 0; i < rows.length; i++){
        addCells(rows.item(i), ncells);
    }
}

/**
 * Crea un objeto de la clase especificada
 * @param type Row/Div
 * @returns {HTMLElement}
 */
function createItem(type) {
    var item = document.createElement("div");
    item.setAttribute("class", type);

    if(type === "cell")
    {
        var id = getNextId();
        item.setAttribute("id", id);
        cells_array.push({
            value: values_array.pop(),
            status: "folded"
        })
    }

    return item;
}

/**
 * Regresa el próximo ID disponible.
 * @returns {number}
 */
function getNextId() {
    cells_ids.push(cells_ids[cells_ids.length-1] + 1);
    return cells_ids[cells_ids.length-1];
}

function getId(cell) {
    return cell.getAttribute("id");
}

function getStatus(cell) {
    return cells_array[getId(cell)].status;
}

function getValue(cell) {
    return cells_array[getId(cell)].value;
}

function setStatus(cell, status) {
    cells_array[getId(cell)].status = status;
    updateClass(cell);
}

function setOnLeave(cell) {
    cell.setAttribute("onmouseleave", 'eventMouseLeaved()');
}

function removeOnLeave(cell) {
    cell.setAttribute("onmouseleave", '');
}

function eventMouseLeaved() {
removeOnLeave(current_pair[0]);
removeOnLeave(current_pair[1]);
    refold();

}

function openCell(cell) {
    setStatus(cell, "opened");
    var item = document.createElement("h1");
    item.innerText = getValue(cell);
    cell.appendChild(item);
}

function onCellClicked(cell) {

    openCell(cell);


    if(current_pair.length === 2)
        refold();

    current_pair.push(cell);



    if(current_pair.length === 2)
    {
        checkPair();
    }


    if (checkGame())
        endGame();


}

function endGame() {
    alert("Finished");
}

function checkGame() {
    return document.getElementsByClassName("unfolded").length === cells_array.length-1;
}

function checkPair() {
    if( getValue(current_pair[0]) === getValue(current_pair[1])){
        setStatus(current_pair[0], "unfolded");
        setStatus(current_pair[1], "unfolded");

        current_pair = [];
    }else{
        setOnLeave(current_pair[1]);
    }
    if(cells_array.length === unfolded.length){
        endGame();
    }
    //refold();
}

function removeChildDom(element) {
    while (element.firstChild){
        element.removeChild(element.firstChild);
    }
}

function refold() {
    setStatus(current_pair[0], "");
    setStatus(current_pair[1], "");

    removeChildDom(current_pair[0]);
    removeChildDom(current_pair[1]);

    current_pair = [];
}

function updateClass(cell) {
    cell.setAttribute("class", "cell " + getStatus(cell));
}

function generateValues() {
    for (var i = 0; i < (NROWS * NCELLS) / 2; i++) {
      values_array.push(i);
      values_array.push(i);
    }

    values_array = shuffle(values_array);
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function callback(e) {
    e = window.e || e;
    var cell_class = e.target.getAttribute("class");

    if(!cell_class)
        return;

    if (cell_class.substr(0,4) !== "cell")
        return;

    if (cell_class.includes("unfolded") || cell_class.includes("opened"))
        return;

    onCellClicked(e.target);
}

if (document.addEventListener)
    document.addEventListener('click', callback);
else
    document.attachEvent('onclick', callback);